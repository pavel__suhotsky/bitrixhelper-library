<?php
namespace Suhotskypavel\Bitrixhelper;

class CSmartbit{
    public function _construct(){
    }

    /**
        * Склонение слова
        *
        * @param - $NUMBER - int - число
        * @param - $WORDS - array - класс, класса, классов
        * @param - $SHOW - bool - Включает значение $value в результирующею строку
        *
        * @return string - строка
    */
    public static function WordCorrect($NUMBER, $WORDS, $SHOW = true){
        $num = $NUMBER % 100;
        if ($num > 19) { 
            $num = $num % 10; 
        }
        
        $out = ($SHOW) ?  $NUMBER . ' ' : '';
        switch ($num) {
            case 1:  $out .= $WORDS[0]; break;
            case 2: 
            case 3: 
            case 4:  $out .= $WORDS[1]; break;
            default: $out .= $WORDS[2]; break;
        }
        
        return $out.'!!';
    }


}
?>