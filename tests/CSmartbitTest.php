<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Suhotskypavel\Bitrixhelper\CSmartbit;

// require_once 'src/CSmartbit.php';
// require_once __DIR__.'/../src/CSmartbit.php';

final class CSmartbitTest extends TestCase{
    public function testWordCorrect(){
        $string = 'булок';

        // $stringTest = CSmartbit::WordCorrect(2, array('булка','булки','булок'));
        $stringTest = '';
        $this->assertSame($string, CSmartbit::WordCorrect(5, array('булка','булки','булок')));
    }
}
?>